# Implementing a CI / CD Pipeline with CakePHP, Docker and Gitlab
Docker is a tool which allows you to package projects in a container, similar to a virtual machine, making it easier to control the development environment for a project across machines. 

For the purposes of this documentation we will assume that we're doing everything in Docker containers. For the setup of any of these tools without a Docker container, see their official documentation--usually it is pretty straightforward.
## Installing Docker
Docker is provided with many package repositories, but it is better to google "Get Docker CE for" along with the name of your operating system as opposed to using what is available with the repository. Reading Docker's official installation instructions will make sure you are most up to date.
The following instructions are copied for [Docker's official documentation for installing Docker CE on CentOs](https://docs.docker.com/install/linux/docker-ce/centos/#uninstall-old-versions).
### Uninstall Old Versions
    sudo yum remove docker \
        docker-client \
        docker-client-latest \
        docker-common \
        docker-latest \
        docker-latest-logrotate \
        docker-logrotate \
        docker-selinux \
        docker-engine-selinux \
        docker-engine     
### Set Up the Repository
This will set up the repository for the most recent stable edition of Docker. For edge releases see the official documentation.

    sudo yum install -y yum-utils \
        device-mapper-persistent-data \
        lvm2
    sudo yum-config-manager \
        --add-repo \
        https://download.docker.com/linux/centos/docker-ce.repo
### Install Docker Community Edition
    sudo yum install docker-ce
### Starting Docker
You may need to start the Docker service.

    systemctl start docker
## How Docker works
Docker uses a repository of container images called [Docker Hub](https://hub.docker.com/). Let's look at an example.

    docker run -d -p 80:80 michaelcharles/hello-world:latest

Docker will first look for whether or not it has ever downloaded the image for "michaelcharles/hello-world" before. If it has not, if it isn't available locally, it will then look for it on Docker Hub. The first part of the image name before the slash refers to the user who added the image to Docker Hub. The next part, "hello-world", is the name of the repository. Finally, after the colon, is the tag you are using. You use tags to specific specific versions of an image that you want to pull.  If you don't specific a tag, it will default to "latest".

The "-d" tag runs the container in detached mode. Without using this tag, the console will show output from PID1 (process ID 1), the main process of the container (incidentally, it is good to know that when PID1 stops, the container does too). In this case PID1 is nginx. 

The -p tag tells Docker how to forward the ports. The first number represents the port number on the machine, and the second number represents the port number on the container. Right now traffic to port 80 on the machine is being directed to port 80 on the container. However if you were to run `docker run -d -p 8080:80 michaelcharles/hello-world` instead, you'd access port 80 on the container by navigating to port 8080 on your machine. 

There are a lot of available tags for configuring Docker, but instead of using those we're going to use a Docker-Compose file to manage our containers. 
### Stopping, Listing and Removing Containers
To list running containers, we use the command `docker container ls`. To list all containers, even ones that are stopped, we use `docker container ls -a`.

Using `docker container ls` will show output similar to the following.

    CONTAINER ID        IMAGE                               COMMAND                  CREATED             STATUS              PORTS                NAMES
    2164bcf28ce0        michaelcharles/hello-world:latest   "nginx -g 'daemon of…"   16 minutes ago      Up 16 minutes       0.0.0.0:80->80/tcp   quirky_mayer
In order to manipulate a container from the command line, you need to use either the container's randomly generated ID, or the container's name. You can specify a container name when you run it by using the `--name` tag, but if you don't one will be randomly generated based on an adjective and the name of a famous scientist. 

Top stop this container we can either use `docker container stop 216` or `docker container stop quicky_mayer`. If you choose to use the container ID, you only need to write enough of it to differentiate between other container IDs. If you use the container name you must type the whole container name. 
### Docker Volumes and Networks and How to Manipulate Them
Docker creates volumes as a way to save persistent data between sessions, and uses networks to communicate between containers and your machine. 
You can list and manipulate containers and volumes using similar commands to containers. `docker volume ls` and `docker network ls` will list networks and volumes respectively, while `docker volume rm volume-name` will remove a volume assuming "volume-name" is the full name of an existing volume.

## Using a Dockerfile

Using a Dockerfile allows you to write your own version of an image. You essentially create a script to help Docker build the image to your own specifications. 

The following is a simple example of setting up Apache running on an Ubuntu container. Simply create a file called "Dockerfile" containing the following.

    FROM ubuntu:latest
    RUN apt-get update && install -y \
        apache2
    RUN echo "Hello World" > /var/www/html/index.html
    CMD /usr//sbin/apache2ctl -D FOREGROUND

To build this image use the following command,

    docker build -t "hello-apache" .

The "-t" flag allows you to tag the image with a new name. We're calling this image "hello-apache". The period at the end tells Docker to look for "Dockerfile" within the current working directory.

Now let's look through the Dockerfile line by line. `FROM ubuntu:latest` tells Docker to start by pulling the latest image of Ubuntu from Docker Hub. This will be the foundation from which the rest of the custom image will be built. Realistically, there are already plenty of simple premade images for running Apache on Docker Hub. For example, we could do `FROM httpd:latest` to pull one of the official Apache images with Apache already installed, but for the sake of showing how Dockerfiles work we will be installing Apache manually. 

The next line is `RUN apt-get update && install -y apache2`. As you might image, `RUN` lets run shell commands from within the container. This tells Docker to update apt-get and then install apache2. It is always necessary to run apt-get update first, as the Ubuntu image that Docker installs won't have any package lists out of the box. 

Then we do `RUN echo "Hello World" > /var/www/html/index.html`. This creates an html file for apache to serve up. I used echo for expedience sake, but there are two major ways that we can get our own project files with the container.

One way is to use `COPY` or `ADD`. `COPY` and `ADD` are very similar. The main difference is that `ADD` allows you to add files via url, whereas `COPY` does not. Common practice is to place your project files in a subdirectory of the folder where the Dockerfile is stored. If we had our project files in a directory named `app`, we could copy them to `/var/www/html` inside the container by using `COPY app /var/www/html`

The second way is to specify a volume when you run the container. When using `ADD` or `COPY`, you are putting the files you specify into the image. When you use a volume, you are creating a link between a folder in your container with a folder on your machine. Running the container by using `docker run -d -v ./html:/var/www/html "hello-apache" .` will cause the subdirectory `html` to be linked with `/var/www/html` inside the container.

Volumes are a way to add persistence to containers. Changes made to `/var/www/html` inside the container will be also made to files within `html` on the host machine. This is not the case when using `COPY` or `ADD`.

Another difference is that volumes are only available when the container is running. They are not available when building the container. So for example, if you wanted to do `RUN python app/app.py` during the build, you could not use a volume to supply the `app.py` file.

## Pushing Images to Docker Hub

After building your Dockerfile, it is possible to push the generated image to Docker Hub. This will allow you to use the image without a Dockerfile, or use it as the foundation for another Dockerfile. To begin, make an account on Docker Hub. Then run the following to login.

    docker login
You will then want to tag your image appropriately for use on Docker Hub. This is how I'd tag the previous example image to upload to my own Docker Hub account.

    docker tag hello-apache michaelcharles/hello-apache
Finally you push your image to Docker Hub.

    docker push michaelcharles/hello-apache

## What is Docker Compose
Docker Compose is a tool which helps facilitate running multiple Docker containers together. One common configuration is to have one container handle your web server (nginx, apache etc), while the other handles the database.
## Installing Docker Compose

Installing docker-compose is relatively simple. First copy the necessary files.

    curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

Then apply the right permissions to the binary.

    chmod +x /usr/local/bin/docker-compose

## How to use Docker Compose

From this point on we'll largely be refering to the files within this repository. If you haven't done so already, go ahead and clone this repository to your local machine.

    git clone https://gitlab.com/michaelcharles/dockerized-cake.git

You'll notice that in the root directory of the project, there is a file called `docker-compose.yml`. We'll go over it in detail in the next major section, but for now just know that it tells Docker Compose what images to pull, what containers to spin up and how to configure them.

Navigate to the same folder as `docker-compose.yml` and run `docker-compose build`. This will make Docker Compose pull the necessary images and set up the containers as they need to be. This will involve pulling two different linux images as well as installing, updating and configuring Apache, PHP and MySQL to run on them, so this might take a while. The good thing about Docker is that after you build the image once, it will use its cache to build it the second time, meaning the next time you need to run this command it'll be significantly shorter.

Next you can run `docker-compose up -d` to run the containers in detached mode. 

Now, if you do `docker container ls` you should see something like this.

    5cbdf5060c74        dockerized-cake_app   "docker-php-entrypoi…"   3 seconds ago       Up Less than a second   0.0.0.0:80->80/tcp   app
    a1ea6eafb018        dockerized-cake_db    "docker-entrypoint.s…"   5 seconds ago       Up 3 seconds            3306/tcp             db

In order to take down these containers you can run `docker-compose down`.

## Our `docker-compose.yml` File, Line-by-line

Take a look at the [`docker-compose.yml` file included in this repository](https://gitlab.com/michaelcharles/dockerized-cake/blob/master/docker-compose.yml).

	version: '3'

This tells docker-compose what version of the `docker-compose.yml` file format we are using. To read about Compose versions see the official documentation [here](https://docs.docker.com/compose/compose-file/compose-versioning/). We're using version 3.

	services:

We begin listing our services. In this file we have two services, `app` and `db`. They're essentially our containers, however the service names don't necessarily correspond to the container names. The service name is what docker-compose uses to differentiate between the two containers, whereas the container name is the name which you would use to manipulate the containers if you were using regular `docker` commands. 

	  app:

`app` is our first service.

	    build:
	        context: ./
	        dockerfile: ./php-apache/Dockerfile

`build` tells Compose what to build. Context serves two purposes: Telling Compose where to find the Dockerfile, and also serving as your working directory on the host machine when executing the Dockerfile. If the Dockerfile happens to be in a different directory from the directory which you want to use as your build context, you can specify it seperately. We've done this here so that our php-apache Dockerfile can make reference to the `cakephp` folder found in the root directory of our project.

	    container_name: "app"

We've set the container name to `app` as well. This will let us manipulate the container using docker commands such as `docker stop app`.

	    ports:
	      - '80:80'

This is pretty straightforward. It sets up port forwarding much in the same way that the "-p" flag did when using Docker.

	    depends_on:
	      - db

This tells docker-compose that `app` depends on `db`, and therefore it should load `db` before loading `app`.

	    links:
	      - db

This has to do with the way that docker-compose links containers for the purposes of using MySQL.

	    restart: always
	    
This tells the container to always restart if it crashses.

	  db:
	  
Now we're at the start of our `db` service. 

	    build:
	        context: ./sql/

For `db` we're only specifying a build context since our Dockerfile is found inside the `sql` folder.

	    container_name: 'db'
	    restart: always
	    command: mysqld --character-set-server=utf8

`command` sets (and overrides the default) command that is first run by the container. Remember that this is going to be PID1, and the container will stop whenver this process stops. Here we're specifying a flag to use utf8 character set.

	    environment:
	      MYSQL_DATABASE: 'cakephp'
	      MYSQL_USER: 'root'
	      MYSQL_ALLOW_EMPTY_PASSWORD: 'yes'

This sets the container's environment variables. These will be used to initiate the MySQL server.

	    volumes:
	      - mysql-data:/var/lib/mysql
	volumes:
	  mysql-data:
	    driver: local
	    
This is a bit different than the usage of volumes we mentioned earlier in this documentation. This volume isn't connecting a folder within your project directory, but is instead a volume built by Docker which exists within the container's write layer.

### Our Project's Dockerfiles.

Our project has two Dockerfiles. The `app` Dockerfile is found at [`./php-apache/Dockerfile`](https://gitlab.com/michaelcharles/dockerized-cake/blob/master/php-apache/Dockerfile) and the `db` Dockerfile is found at [`./sql/Dockerfile`](https://gitlab.com/michaelcharles/dockerized-cake/blob/master/sql/Dockerfile).

The Dockerfile for `db` simply specifies that we're pulling from the mysql:5.5 image, and then copies over an sql dump to set up the tables that we need to run our dummy CakePHP application.

The Dockerfile for `app` sets up a ubuntu container running php and apache, as well as installing various necessary tools such as Composer.

## Gitlab & CI / CD Pipeline

CI stands for *continuous integration*, and CD stands for *continuous delivery*. A CI / CD Pipeline provides a way for developers to work on a project in multiple test environments, making incremental changes and providing an automated system for delivery to the production environment. Gitlab comes with a built in CI / CD Pipeline solution. Once a change is pushed to a Gitlab repository, Gitlab automatically looks for Gitlab Runners which are tagged appropriately to test that project. If one is found, Gitlab automatically runs (and tests) the project on the machine that the Gitlab Runner is hosted on.

### Shared Runners vs. Specific Runners

In short, a Shared Runner can be used by more than one project, whereas a Specific Runner is a runner configured to work with a specific project. Gitlab provides a range of free Shared Runners that you can use for testing purposes. For our purposes however, we want to have a Specific Runner set up to deploy our CakePHP project inside our test environment.

### Setting up our Gitlab Runner

Helpfully, there happens to be an official Docker implementation of Gitlab Runner. Unfortunately, the container it creates is itself not set up to spin up sibling Docker containers. 

I've put together my own implementation of gitlab-runner running inside a Docker container with Docker installed in it, using volumes to allow the service of Docker inside the container to spin up containers on the host machine. You can clone it with the following command.

    git clone https://gitlab.com/michaelcharles/docker-runner.git

Once you've cloned the repository, navigate to the project's root directory and run,

    docker-compose up -d

After the image builds and the container is running, execute the register command with the following.

    docker exec -it gitlab-runner gitlab-runner register

You will then be prompted to provide the gitlab-ci coordinator URL. If you are not self hosting an instance of Gitlab, then this will be `https://gitlab.com/`.

Next you'll be asked for a gitlab-ci token. You can get this by navigating in the Gitlab sidebar to Settings > CI / CD. Look under the section titled "Runners," and then the "Specific Runners" section. Your gitlab-ci token will be provided there. 

You can then enter a description for your runner. This can be literally anything. This is just to help you differentiate between your runners. I'm going to use the description Terry Crews [because he's awesome](https://www.youtube.com/watch?v=0hwpDnA0_V8).

After that, you will be asked to enter gitlab-ci tags for the runner. These tags are used by Gitlab's CI /CD Pipeline service to decide which runner to use for which project. We're going to use the tag "docker-runner", but the tag can really be anything so long as it is consistent with the tag specified within the `.gitlab-ci.yml` file for the project.

Next you will be asked to enter the "executor" for our project. We'll use "shell".

### Setting up .gitlab-ci.yml
The `.gitlab-ci.yml` file of a project tells Gitlab's CI / CD Pipeline service what to do with the project after changes have been pushed to it. 
Ours is very simply right now, and just looks like this,

	before_script:
	  - docker info
     
    build_image:
	  tags:
	    - docker-runner
	  script:
	    - docker-compose build
	    - docker-compose up -d

Every command we write in `script:` is executed by the runner when changes are pushed to the repository. In our file we're simply building and bringing up our `app` and `db` containers. Everything in `before_script` gets run before that. We're running `docker info` to make sure that Docker is running and working on the container before proceeding.

The `tags` section under `build_image:` tells Gitlab to search for Gitlab Runners also tagged with `docker-runner`.