FROM php:7.1-apache

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    libmcrypt-dev \
    apache2-bin \
    curl \
    git \
    vim \
    libxml2-dev \
    libicu-dev \
    elinks

RUN docker-php-ext-install \ 
    pdo_mysql \
    mysqli \
    mbstring \
    mcrypt \
    gd \
    iconv \
    zip \
    xml \
    intl

# Set environmental variables
ENV COMPOSER_HOME /root/composer
# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ADD ./php-apache/php.ini /usr/local/etc/php/php.ini

ADD ./php-apache/default.conf /etc/apache2/sites-available/000-default.conf

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP root
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2

RUN mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR
RUN a2enmod rewrite

COPY cakephp /var/www/html

RUN composer install

CMD ["apache2", "-D", "FOREGROUND"]

RUN chown -R www-data /var/www/html/app/tmp
